# Data vizualisation pour BD

Le projet se définit en 3 parties: la conversion des CBML vers Json, la base de données en mongodb et la partie visualisation avec d3.js


Dans le répertoire conversion, on trouve 2 fichiers python: l'un permet de faire la conversion de tous les fichiers stockés sur la machine et l'autre ne fait la conversion que d'un seul fichier.
Dans le fichier conversion_abdera_tous_fichiers.py, il y a la ligne de commande suivante: for path, subdirs, files in os.walk('/mnt/c/Users/debor/Documents/INSA/L3i/CBML_for_Deborah/CBML_for_Deborah')
Il s'agit du chemin où sont stockés les fichiers CBML sur mon ordinateur et il faut la remplacer pour que cela corresponde à son chemin. Il n'est pas nécéssaire de convertir de lancer ce programme à chaque fois, uniquement s'il y a eu des modifications.
Pour lancer ce programme, effectuez la commande suivante: python3 conversion_abdera_tous_fichiers.py
L'autre fichier conversion_abdera_un_fichier.py pernet de ne convertir qu'un seul fichier, ce qui est pratique pour faire des tests. Egalement si un seul fichier a été ajouté, cela sera plus simple et plus rapide de ne convertir qu'un seul fichier.
Pour le lancer, effectuez la commande suivante: python3 conversion_abdera_un_fichier.py
Le programme vous demandera le nom du fichier que vous souhaitez convertir.
Il y a deux autres fichiers qui permettent de montrer le même fichier avant (.cbml) et après conversion (.json).


Dans le répertoire mongodb, on a le fichier insertion.py, qui permet d'insérer les fichiers précédemment convertis dans une database orientée document dans mongodb.
Il prend les fichiers .json dans les dossiers dont on a défini le chemin ('/mnt/c/Users/debor/Documents/INSA/L3i/CBML_for_Deborah/CBML_for_Deborah' sur mon ordinateur personnel).
Le script python permet de se connecter sur le 'localhost:27017' et de charger les fichiers dans la base de données 'databaseCBML', dans la collection 'insert_cbml'. Ces paramètres peuvent être modifiés
Pour le lancer, effectuez la commande suivante: python3 insertion_database.py

Le fichier server.js est le fichier côté serveur pour faire le lien entre la base mongodb et d3. Il permet de se connecter à la base.
Attention, si les paramètres de connexion ont été modifiés dans le script insertion_database.py, il faut les modifier également ici, sinon la connexion ne se fera pas.
Pour le lancer, on rentre node server.js



Dans le répertoire visualisation, on trouve un exemple de visualisation sous d3.js.

Pour le lancer et éviter des problèmes de sécurité, le mieux est de lancer un serveur python dans le répertoire du fichier HTML avec la commande suivante:
python3 -m http.server
et lancer dans un navigateur http://localhost:8000/# VisuBDeborah
