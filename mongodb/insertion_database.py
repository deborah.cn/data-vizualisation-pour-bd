import pymongo
import json
import os, sys


def recup_doc():
    liste_json=[]
    for path, subdirs, files in os.walk('/mnt/c/Users/debor/Documents/INSA/L3i/CBML_for_Deborah/CBML_for_Deborah/'):
        for name in files:
            if name.endswith('.json'):
                liste_json.append(os.path.join(path, name))

    #print(liste_json)
    return(liste_json)


myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["databaseCBML"]
mycol = mydb["insert_cbml"]

liste_json=recup_doc()
print(liste_json)


for file_name in liste_json:
    with open(file_name) as f:
        file_data = json.load(f)  # load data from JSON to dict
        print("j'ai load",file_name)

        x= mycol.insert(file_data, check_keys=False)
